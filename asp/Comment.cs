using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class Comment : API {

		/**
		 * Insert a new comment.
		 *
		 * @POST body	The comment's body.
		 * @POST parent	The comment's parent.
		 * @POST post	The comment's post.
		 *
		 * @return bool
		 */
		public bool POST_Insert() {

			foreach(var parameter in new List<string> {"body", "post"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			Database.Query(
				"INSERT INTO comments (username, body, parent, post) VALUES (@1, @2, @3, @4)",
				new Dictionary<string, string> {
					{"@1", this.Username},
					{"@2", Request.Form["body"]},
					{"@3", Request.Form["parent"]},
					{"@4", Request.Form["post"]}
				}
			);

			return true;
		}

		/**
		 * Update a comment.
		 *
		 * @POST id		The comment's id.
		 * @POST body	The comment's new body.
		 *
		 * @return bool
		 */
		public bool POST_Update() {

			foreach(var parameter in new List<string> {"id", "body"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			Database.Query(
				"UPDATE comments SET body = @1 WHERE id = @2 AND username = @3",
				new Dictionary<string, string> {
					{"@1", Request.Form["body"]},
					{"@2", Request.Form["id"]},
					{"@3", this.Username},
				}
			);

			return true;
		}

		/**
		 * Delete a comment.
		 *
		 * This doesn't really deletes a comment. It just empties the comment and removes the associated user.
		 * This is done to keep the comment heirarchey in place if a non leaf comment is deleted.
		 *
		 * @POST id	The comment's id.
		 *
		 * @return bool
		 */
		public bool POST_Delete() {

			foreach(var parameter in new List<string> {"id"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			Database.Query(
				"UPDATE comments SET body = NULL, username = NULL WHERE id = @1 AND username = @2",
				new Dictionary<string, string> {
					{"@1", Request.Form["id"]},
					{"@2", this.Username},
				}
			);

			return true;
		}

		/**
		 * Vote on a comment.
		 *
		 * This deals with the logic of the vote already being there and what not.
		 *
		 * @POST post	The id the of the comment on which the vote is to be added.
		 * @POST vote	(1|-1) The value of the vote
		 * @return bool
		 */
		public bool POST_Vote() {

			foreach(var parameter in new List<string> {"post", "vote"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			var vote = Request.Form["vote"].Equals("1") ? "1" : "-1";

			var oldvote = Database.Query(
				"SELECT id, vote FROM commentsvotes WHERE username = @1 AND post = @2",
				new Dictionary<string, string> {
					{"@1", this.Username},
					{"@2", Request.Form["post"]}
				}
			);

			// Calculate the new vote
			var newvote = (oldvote.Rows.Count > 0) ? oldvote.Rows[0]["vote"].ToString() : "0";
			newvote = newvote.Equals(vote) ? "0" : vote;


			// If the vote was already added and the new vite is neutral then delete the old vote
			if(oldvote.Rows.Count > 0 && newvote.Equals("0")) {
				Database.Query(
					"DELETE FROM commentsvotes WHERE id = @1",
					new Dictionary<string, string> {{"@1", oldvote.Rows[0]["id"].ToString()}}
				);
			}

			// If the vote was already added and the new vote is not neutral then update the old vote
			else if(oldvote.Rows.Count > 0) {
				Database.Query(
					"UPDATE commentsvotes SET vote = @1 WHERE id = @2",
					new Dictionary<string, string> {
						{"@1", vote},
						{"@2", oldvote.Rows[0]["id"].ToString()}
					}
				);
			}

			// If the vote was never added before then add the new vote
			else {
				Database.Query(
					"INSERT INTO commentsvotes (post, vote, username) VALUES (@1, @2, @3)",
					new Dictionary<string, string> {
						{"@1", Request.Form["post"]},
						{"@2", vote},
						{"@3", this.Username}
					}
				);
			}

			return true;
		}
	}
}