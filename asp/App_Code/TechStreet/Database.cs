using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace TechStreet {
	public class Database {

		public static long LastInsertedId;

		public static DataTable Query(string query, Dictionary<string, string> parameters) {
			string ConnectionString = "server=127.0.0.1;user="+TechStreet.DatabaseConnection.User+";database="+TechStreet.DatabaseConnection.Database+";port=3306;password="+TechStreet.DatabaseConnection.Password+";";

			MySqlConnection Connection = new MySqlConnection(ConnectionString);

			Connection.Open();

			MySqlCommand command = new MySqlCommand(query, Connection);

			if(parameters != null) {
				foreach(var parameter in parameters) {
					command.Parameters.AddWithValue(parameter.Key, parameter.Value);
				}
			}

			var datatable = new DataTable();
			datatable.Load(command.ExecuteReader());

			Database.LastInsertedId = command.LastInsertedId;

			Connection.Close();

			return datatable;
		}
	}
}