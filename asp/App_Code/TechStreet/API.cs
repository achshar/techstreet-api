using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class API : Page {

		protected bool AuthenticateUser = true;

		protected string Username;

		protected string Token;

		protected string ErrorMessage;

		private class ApiResponse {
			public bool Status;
			public object Response;
		}

		protected void Respond(object Data, string ErrorMessage = null) {

			Response.AppendHeader("Access-Control-Allow-Origin", "*");
			Response.ContentType = "application/json";

			var ApiResponse = new API.ApiResponse();

			ApiResponse.Status = Data != null;

			if(ErrorMessage != null)
				this.ErrorMessage = ErrorMessage;

			ApiResponse.Response = (!ApiResponse.Status && this.ErrorMessage != null) ? this.ErrorMessage : Data;

			Response.Write(JsonConvert.SerializeObject(ApiResponse));
			Response.End();
		}

		protected void Page_Load(object sender, EventArgs e) {

			if(this.AuthenticateUser) {
				this.Token = Request.QueryString["token"];

				if(this.Token == null || this.Token.Equals(""))
					Respond(null, "Token Missing! :(");

				if(!ValidToken())
					Respond(null, "Invalid Token! :(");
			}

			var action = Request.QueryString["action"];

			if(action == null || action.Equals(""))
				Respond(null, "Action Missing! :(");

			if(Request.HttpMethod == "POST")
				action = "POST_" + action;

			var method = GetType().GetMethod(action);

			if(method == null)
				Respond(null, "Invalid Action! :(");

			Respond(method.Invoke(this, null));
		}

		private bool ValidToken() {

			var TokenRows = Database.Query(
				"SELECT * FROM tokens WHERE token = @1",
				new Dictionary<string, string> {
					{"@1", this.Token}
				}
			);

			if(TokenRows.Rows.Count == 0)
				return false;

			this.Username = (string) TokenRows.Rows[0]["username"];

			Database.Query(
				"UPDATE tokens SET ip = @1, useragent = @2, lastused = NOW() WHERE token = @3",
				new Dictionary<string, string> {
					{"@1", Request.UserHostAddress},
					{"@2", Request.UserAgent},
					{"@3", this.Token}
				}
			);

			return true;
		}
	}
}