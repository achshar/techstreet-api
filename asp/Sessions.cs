using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class Sessions : API {

		/**
		 * Returns the list of sessions that the user currently is logged into.
		 * @return array
		 */
		public DataTable Get() {

			var sessions = Database.Query(
				"SELECT token, timestamp, lastused, ip, useragent FROM tokens WHERE username = @1 ",
				new Dictionary<string, string> {
					{"@1", this.Username}
				}
			);

			sessions.Columns.Add("current");

			foreach(DataRow session in sessions.Rows) {

				session["current"] = session["token"].Equals(this.Token) ? "true" : "false";

				session.EndEdit();
				sessions.AcceptChanges();
			}

			//sessions.Columns.Remove("token");

			return sessions;
		}

		/**
		 * Log out the user by deleting the user's authentication token.
		 * @return true
		 */
		public bool POST_Purge() {

			Database.Query(
				"DELETE FROM tokens WHERE token = @1 ",
				new Dictionary<string, string> {
					{"@1", this.Token}
				}
			);

			return true;
		}

		/**
		 * Resets all other sessions of the user.
		 * @return bool
		 */
		public bool POST_Reset() {

			Database.Query(
				"DELETE FROM tokens WHERE username = @1 AND token != @2 ",
				new Dictionary<string, string> {
					{"@1", this.Username},
					{"@2", this.Token}
				}
			);

			return true;
		}
	}
}