using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class Posts : API {

		public DataTable Get() {

			var Where = "";
			var parameters = new Dictionary<string, string> {{"@currentusername", this.Username}};

			if(Request.QueryString["username"] != null) {
				Where += "WHERE posts.username = @username ";
				parameters["@username"] = Request.QueryString["username"];
			}

			if(Request.QueryString["id"] != null) {
				Where += "WHERE posts.id = @id ";
				parameters["@id"] = Request.QueryString["id"];
			}

			return Database.Query(
				"SELECT "+
					"posts.*, postsvotes.votes, uservote.vote uservote, COUNT(comments.id) comments "+
				"FROM posts "+
					"LEFT JOIN (SELECT SUM(vote) votes, post FROM postsvotes GROUP BY post) postsvotes ON posts.id = postsvotes.post "+
					"LEFT JOIN postsvotes uservote ON posts.id = uservote.post AND uservote.username = @currentusername "+
					"LEFT JOIN comments ON posts.id = comments.post "+
				Where+
				"GROUP BY posts.id "+
				"ORDER BY postsvotes.votes DESC",
				parameters
			);
		}
	}
}