using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class Profile : API {

		/**
		 * Shows the information about a profile if provided otherwise return logged in user's profile.
		 *
		 * @GET username string (optional) The user whose profile data is needed.
		 * @return array
		 */
		public DataTable Get() {

			var username = Request.QueryString["username"] ?? this.Username;

			var info = Database.Query(
				"SELECT "+
					"users.username, users.email, users.aboutme, "+
					"users.lastlogin, users.timestamp, "+
					"COUNT(DISTINCT posts.id) postcount, "+
					"COUNT(DISTINCT comments.id) commentcount "+
				"FROM users "+
					"LEFT JOIN posts ON users.username = posts.username "+
					"LEFT JOIN comments ON users.username = comments.username "+
				"WHERE users.username = @1 "+
				"GROUP BY users.username",
				new Dictionary<string, string> {
					{"@1", username}
				}
			);

			var linkkarma = Database.Query(
				"SELECT "+
					"SUM(postsvotes.vote) linkkarma "+
				"FROM posts, postsvotes "+
				"WHERE posts.id = postsvotes.post AND posts.username = @1 ",
				new Dictionary<string, string> {
					{"@1", username}
				}
			);

			var commentkarma = Database.Query(
				"SELECT "+
					"SUM(commentsvotes.vote) commentkarma "+
				"FROM comments, commentsvotes "+
				"WHERE comments.id = commentsvotes.post AND comments.username = @1 ",
				new Dictionary<string, string> {
					{"@1", username}
				}
			);

			info.Columns.Add("linkkarma");
			info.Columns.Add("commentkarma");

			info.Rows[0]["linkkarma"] = linkkarma.Rows[0]["linkkarma"];
			info.Rows[0]["commentkarma"] = commentkarma.Rows[0]["commentkarma"];

			return info;
		}

		/**
		 * Delete's the user's profile form the site.
		 *
		 * @POST string password	The user's password which is needed to delete a profile.
		 * @return bool
		 */
		public bool POST_Delete() {

			foreach(var parameter in new List<string> {"password"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			var user = Database.Query(
				"SELECT password FROM users WHERE username = @1",
				new Dictionary<string, string>{
					{"@1", this.Username}
				}
			);

			if(!user.Rows[0]["password"].Equals(Request.Form["password"]))
				Respond(null, "Invalid Password! :(");

			Database.Query(
				"UPDATE postsvotes SET username = NULL WHERE username = @1",
				new Dictionary<string, string>{{"@1", this.Username}}
			);
			Database.Query(
				"UPDATE commentsvotes SET username = NULL WHERE username = @1",
				new Dictionary<string, string>{{"@1", this.Username}}
			);
			Database.Query(
				"DELETE FROM tokens WHERE username = @1",
				new Dictionary<string, string>{{"@1", this.Username}}
			);
			Database.Query(
				"UPDATE posts SET body = NULL, username = NULL WHERE username = @1",
				new Dictionary<string, string>{{"@1", this.Username}}
			);
			Database.Query(
				"UPDATE comments SET body = NULL, username = NULL WHERE username = @1",
				new Dictionary<string, string>{{"@1", this.Username}}
			);
			Database.Query(
				"DELETE FROM users WHERE username = @1",
				new Dictionary<string, string>{{"@1", this.Username}}
			);

			return true;
		}

		/**
		 * Change the user's password provided the user knows the current password.
		 *
		 * @POST oldpassword	The user's current password.
		 * @POST newpassword	The user's new password.
		 *
		 * @return bool
		 */
		public bool POST_ChangePassword() {

			foreach(var parameter in new List<string> {"oldpassword", "newpassword"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			var user = Database.Query(
				"SELECT password FROM users WHERE username = @1",
				new Dictionary<string, string>{
					{"@1", this.Username}
				}
			);

			if(!user.Rows[0]["password"].Equals(Request.Form["oldpassword"]))
				Respond(null, "Invalid Password! :(");

			Database.Query(
				"UPDATE users SET password = @1 WHERE username = @2",
				new Dictionary<string, string>{
					{"@1", Request.Form["newpassword"]},
					{"@2", this.Username}
				}
			);

			return true;
		}
	}
}