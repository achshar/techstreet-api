using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class post : API {

		/**
		 * Insert a new post.
		 *
		 * @POST title  The title of the post.
		 * @POST url    The url of the post.
		 * @POST body   The body of the post.
		 * @return long The new post's id if it was successfully added otherwise false.
		 */
		public long POST_Insert() {

			foreach(var parameter in new List<string> {"title", "url", "body"}) {
				if(Request.Form[parameter] == null)
					Respond(null, "Incomplete Request! :(");
			}

			if(Request.Form["title"].Equals("") || (Request.Form["url"].Equals("") && Request.Form["body"].Equals("")))
				Respond(null, "Incomplete Request! :(");

			Database.Query (
				"INSERT INTO posts (title, url, body, username) VALUES (@1, @2, @3, @4)",
				new Dictionary<string, string> {
					{"@1", Request.Form["title"]},
					{"@2", Request.Form["url"]},
					{"@3", Request.Form["body"]},
					{"@4", this.Username}
				}
			);

			return Database.LastInsertedId;
		}


		/**
		 * Delete a post.
		 *
		 * This doesn't really delete a post because that would also mean the post's comments become orphan.
		 * So instead we just update the post to remove the body and it's associated username.
		 *
		 * @POST id	The id of the post that needs to be deleted.
		 * @return bool
		 */
		public bool POST_Delete() {

			foreach(var parameter in new List<string> {"id"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			Database.Query (
				"UPDATE posts SET body = NULL, username = NULL WHERE id = @1 AND username = @2",
				new Dictionary<string, string> {
					{"@1", Request.Form["id"]},
					{"@2", this.Username}
				}
			);

			return true;
		}

		/**
		 * Vote on a post.
		 *
		 * This deals with the logic of the vote already being there and what not.
		 *
		 * @POST post	The id the of the post on which the vote is to be added.
		 * @POST vote	(1|-1) The value of the vote
		 * @return bool
		 */
		public bool POST_Vote() {

			foreach(var parameter in new List<string> {"post", "vote"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			var vote = Request.Form["vote"].Equals("1") ? "1" : "-1";

			var oldvote = Database.Query(
				"SELECT id, vote FROM postsvotes WHERE username = @1 AND post = @2",
				new Dictionary<string, string> {
					{"@1", this.Username},
					{"@2", Request.Form["post"]}
				}
			);

			// Calculate the new vote
			var newvote = (oldvote.Rows.Count > 0) ? oldvote.Rows[0]["vote"].ToString() : "0";
			newvote = newvote.Equals(vote) ? "0" : vote;


			// If the vote was already added and the new vite is neutral then delete the old vote
			if(oldvote.Rows.Count > 0 && newvote.Equals("0")) {
				Database.Query(
					"DELETE FROM postsvotes WHERE id = @1",
					new Dictionary<string, string> {{"@1", oldvote.Rows[0]["id"].ToString()}}
				);
			}

			// If the vote was already added and the new vote is not neutral then update the old vote
			else if(oldvote.Rows.Count > 0) {
				Database.Query(
					"UPDATE postsvotes SET vote = @1 WHERE id = @2",
					new Dictionary<string, string> {
						{"@1", vote},
						{"@2", oldvote.Rows[0]["id"].ToString()}
					}
				);
			}

			// If the vote was never added before then add the new vote
			else {
				Database.Query(
					"INSERT INTO postsvotes (post, vote, username) VALUES (@1, @2, @3)",
					new Dictionary<string, string> {
						{"@1", Request.Form["post"]},
						{"@2", vote},
						{"@3", this.Username}
					}
				);
			}

			return true;
		}
	}
}
