using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace TechStreet {
	public class Comments : API {

		public DataTable Get() {

			var Where = "";
			var parameters = new Dictionary<string, string> {{"@currentusername", this.Username}};

			if(Request.QueryString["username"] != null) {
				Where += "WHERE comments.username = @username ";
				parameters["@username"] = Request.QueryString["username"];
			}

			if(Request.QueryString["post"] != null) {
				Where += "WHERE comments.post = @post ";
				parameters["@post"] = Request.QueryString["post"];
			}

			return Database.Query(
				"SELECT "+
					"comments.*, commentsvotes.votes, uservote.vote uservote, posts.id post, posts.title posttitle "+
				"FROM comments "+
					"LEFT JOIN (SELECT SUM(vote) votes, post FROM commentsvotes GROUP BY post) commentsvotes ON comments.id = commentsvotes.post "+
					"LEFT JOIN commentsvotes uservote ON comments.id = uservote.post AND uservote.username = @currentusername "+
					"LEFT JOIN posts ON comments.post = posts.id "+
				Where+
				"GROUP BY comments.id "+
				"ORDER BY votes DESC ",
				parameters
			);
		}
	}
}