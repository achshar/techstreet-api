using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;

using System.Security.Cryptography;
using System.Text;

using Newtonsoft.Json;

namespace TechStreet {
	public class User : API {

		public User() {
			this.AuthenticateUser = false;
		}

		private string RandomString(int size) {
			var chars = new char[62];
			chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
			var data = new byte[1];

			using(RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider()) {
				crypto.GetNonZeroBytes(data);
				data = new byte[size];
				crypto.GetNonZeroBytes(data);
			}

			StringBuilder result = new StringBuilder(size);
			foreach (byte b in data) {
				result.Append(chars[b % (chars.Length)]);
			}

			return result.ToString();
		}

		/**
		 * The section registers a new user provided the username is unique.
		 *
		 * @POST username
		 * @POST password
		 *
		 * @return bool The status of the registration request
		 */
		public bool POST_Register() {

			foreach(var parameter in new List<string> {"username", "password"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			var user = Database.Query(
				"SELECT * FROM users WHERE username = @username",
				new Dictionary<string, string> {
					{"@username", Request.Form["username"]}
				}
			);

			if(user.Rows.Count > 0)
				Respond(null, "Username already registered! :(");

			Database.Query(
				"INSERT INTO users (username, password) VALUES (@1, @2)",
				new Dictionary<string, string> {
					{"@1", Request.Form["username"]},
					{"@2", Request.Form["password"]}
				}
			);

			return true;
		}

		/**
		 * Authenticate the user provided the username and password and return an identification token.
		 *
		 * @POST username
		 * @POST password
		 * @return bool|string The identification token on success or false on failure.
		 */
		public object POST_Login() {

			foreach(var parameter in new List<string> {"username", "password"}) {
				if(Request.Form[parameter] == null || Request.Form[parameter].Equals(""))
					Respond(null, "Incomplete Request! :(");
			}

			var user = Database.Query(
				"SELECT * FROM users WHERE username = @username",
				new Dictionary<string, string> {
					{"@username", Request.Form["username"]}
				}
			);

			if(user.Rows.Count == 0)
				Respond(null, "Invalid Username! :(");

			if(!user.Rows[0]["password"].Equals(Request.Form["password"]))
				Respond(null, "Invalid Password! :(");

			var token = RandomString(20);

			Database.Query(
				"INSERT INTO tokens (username, token, ip, useragent) VALUES (@1, @2, @3, @4)",
				new Dictionary<string, string> {
					{"@1", Request.Form["username"]},
					{"@2", token},
					{"@3", Request.UserHostAddress},
					{"@4", Request.UserAgent}
				}
			);

			return token;
		}
	}
}