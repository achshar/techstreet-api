<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

// Send the json header
header('Content-Type: application/json');

function respond($output, $ErrorMessage = false) {

	$status = (boolean) $output;

	$response = [
		'status' => $status,
		'response' => (!$status && $ErrorMessage) ? $ErrorMessage : $output
	];

	// Pretty print the JSON if requested
	if(isset($_GET['debug']))
		exit(json_encode($response, JSON_PRETTY_PRINT));
	else
		exit(json_encode($response));
}

$mysql = new mysql;

/**
 * The section registers a new user provided the username is unique.
 *
 * @POST username
 * @POST password
 *
 * @return bool The status of the registration request
 */
if(isset($_GET['action']) && $_GET['action'] == 'register') {

	// Make sure all the required fields are provided
	foreach(['username', 'password'] as $key) {
		if(!isset($_POST[$key]) && !$_POST[$key])
			respond(false, 'Not enough information was provided!');
	}

	// Get a user with either the provided email or username
	$mysql->query(
		'SELECT * FROM users WHERE username = ?',
		[$_POST['username']]
	);

	// If the above user was found then we have a collision
	if($mysql->rows)
		respond(false, 'User Already Exists!');

	// If no problems were encountered then add the user
	$mysql->query(
		'INSERT INTO users (username, password, email, aboutme) VALUES (?, ?, ?, ?)',
		[$_POST['username'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['email'], $_POST['aboutme']]
	);

	// Return true since the registration was successful
	respond(true);
}

/**
 * Authenticate the user provided the username and password and return an identification token.
 *
 * @POST username
 * @POST password
 * @return bool|string The identification token on success or false on failure.
 */
if(isset($_GET['action']) && $_GET['action'] == 'login') {

	// Make sure all the required and non null fields are provided
	foreach(['username', 'password'] as $key) {
		if(!isset($_POST[$key]) || !$_POST[$key])
			respond(false, 'Not enough information was provided!');
	}

	// Make sure the passed username is legit and get the user's password hash
	$user = $mysql->query(
		"SELECT username, password, email FROM users WHERE email = ? OR username = ?",
		[$_POST['username'], $_POST['username']]
	);

	// If the user wasn't found
	if(!$user)
		respond(false, 'Invalid User!');

	$user = $user[0];

	// If the password was not verified
	if(!password_verify($_POST['password'], $user['password']))
		respond(false, 'Invalid Password!');

	// Get a random token to identify the login across sessions
	$token = RandomString(20);

	// Make sure the token is unique
	while($mysql->query('SELECT id FROM tokens WHERE token = ?', [$token]))
		$token = RandomString(20);

	// Save the identifier token with the user's profile for later authentication
	$mysql->query(
		'INSERT INTO tokens (username, token, ip, useragent) VALUES (?, ?, ?, ?)',
		[$user['username'], $token, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']]
	);

	respond($token);
}

respond(false, 'Invalid Action!');