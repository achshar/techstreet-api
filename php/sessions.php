<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

class sessions extends API {

	/**
	 * Returns the list of sessions that the user currently is logged into.
	 * @return array
	 */
	protected function get() {

		// TODO: Add the user agent parsing and return
		return $this->mysql->query('SELECT lastused, ip FROM tokens WHERE username = ?', [$this->username]);
	}

	/**
	 * Log out the user by deleting the user's authentication token.
	 * @return true
	 */
	protected function POST_purge() {
		$this->mysql->query('DELETE FROM tokens WHERE token = ?', [$this->token]);

		return true;
	}

	/**
	 * Resets all other sessions of the user.
	 * @return bool
	 */
	protected function POST_reset() {
		$this->mysql->query('DELETE FROM tokens WHERE username = ? AND token != ?', [$this->username, $this->token]);

		return true;
	}
}

new sessions;
?>