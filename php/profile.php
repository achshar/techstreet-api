<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

class profile extends API {

	/**
	 * Shows the information about a profile if provided otherwise return logged in user's profile.
	 *
	 * @GET username string (optional) The user whose profile data is needed.
	 * @return array
	 */
	protected function get() {
		$username = isset($_GET['username']) ? $_GET['username'] : $this->username;

		$rows = $this->mysql->query(
			'SELECT
				users.username, users.email, users.aboutme,
				users.lastlogin, users.timestamp,
				SUM(postsvotes.vote) linkkarma,
				SUM(commentsvotes.vote) commentkarma
			FROM users
				LEFT JOIN posts ON users.username = posts.username
				LEFT JOIN postsvotes ON posts.id = postsvotes.post
				LEFT JOIN comments ON users.username = comments.username
				LEFT JOIN commentsvotes ON comments.post = commentsvotes.post
			WHERE users.username = ?
			GROUP BY users.username',
			[$username]
		);

		foreach($rows as &$row) {
			$row['timestamp'] = strtotime($row['timestamp']);
		}

		return $rows;
	}

	/**
	 * Update a user's profile information
	 *
	 * @POST email		The user's email.
	 * @POST aboutme	The user's about-me content.
	 *
	 * @return bool
	 */
	protected function POST_update() {

		// Make sure all the required fields are provided
		foreach(['email', 'aboutme'] as $key) {
			if(!isset($_POST[$key]))
				return false;
		}

		if($_POST['email']) {

			// If the email is not a valid syntax
			if(!filter_var($email, FILTER_VALIDATE_EMAIL))
				return false;

			// Make sure some other user doesn't already have the email in their profile
			if($this->mysql->query('SELECT id FROM users WHERE email = ? AND username != ?', [$_POST['email'], $this->username]))
				return false;
		}

		// If everything looks fine above then update the user profile
		$this->mysql->query('UPDATE users SET email = ? AND aboutme = ?', [$_POST['email'], htmlentities($_POST['aboutme'], ENT_QUOTES)]);

		return true;
	}

	/**
	 * Delete's the user's profile form the site.
	 *
	 * @POST string password	The user's password which is needed to delete a profile.
	 * @return bool
	 */
	protected function POST_delete() {

		// Make sure the password was provided
		if(!isset($_POST['password']))
			return false;

		// Get the user's current password
		$user = $this->mysql->query('SELECT password FROM users WHERE username = ?', [$this->username]);

		if(!$user || !$user = $user[0])
			return false;

		// If the provided password was not valid
		if(!password_verify($_POST['password'], $user['password']))
			return false;

		// If everything above looks fine then start deleting user's information
		$this->mysql->query('DELETE FROM votes WHERE username = ?', [$this->username]);
		$this->mysql->query('DELETE FROM tokens WHERE username = ?', [$this->username]);
		$this->mysql->query('UPDATE posts SET body = NULL, username = NULL WHERE username = ?', [$this->username]);
		$this->mysql->query('UPDATE comments SET body = NULL, username = NULL WHERE username = ?', [$this->username]);
		$this->mysql->query('DELETE FROM users WHERE username = ?', [$this->username]);

		return true;
	}

	/**
	 * Change the user's password provided the user knows the current password.
	 *
	 * @POST oldpassword	The user's current password.
	 * @POST newpassword	The user's new password.
	 *
	 * @return bool
	 */
	protected function POST_changepassword() {

		// Make sure all the required fields are provided
		foreach(['oldpassword', 'newpassword'] as $key) {
			if(!isset($_POST[$key]) || !$_POST[$key])
				return false;
		}

		// Get the user's current password
		$user = $this->mysql->query('SELECT password FROM users WHERE username = ?', [$this->username]);

		if(!$user || !$user = $user[0]) {
			$this->ErrorMessage = 'Invalid User!';
			return false;
		}

		// If the provided password was not valid
		if(!password_verify($_POST['oldpassword'], $user['password'])) {
			$this->ErrorMessage = 'Invalid Password!';
			return false;
		}

		// If everything looks fine above then update the password
		$this->mysql->query(
			'UPDATE users SET password = ? WHERE username = ?',
			[password_hash($_POST['newpassword'], PASSWORD_DEFAULT), $this->username]
		);

		return true;
	}
}

new profile;
?>