<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

class posts extends API {

	/**
	 * Show posts given, narrowed down by username if given.
	 *
	 * @GET username	string 	(optional) The user whose posts to fetch.
	 * @GET orderby		string 	(time|votes) The type of sorting, by time or by votes.
	 * @GET order		string 	(asc|desc) Ascending or descending sorting.
	 * @GET start		int 	The starting position in the sorted list. Used for pagination.
	 * @GET count		int 	The number of items to show.
	 * @return array|bool
	 */
	protected function get() {
		foreach(['orderby', 'order', 'start', 'count'] as $value) {
			if(!isset($_GET[$value]))
				return false;
		}

		if(!(int) $_GET['count'])
			return false;

		// Prepare a where where array to store the filters
		$where = [];
		if(isset($_GET['username']) && $_GET['username'])
			$where['username'] = $_GET['username'];

		// If the sorting order is time then simply show the profile's posts
		if($_GET['orderby'] == 'time') {
			$rows = $this->mysql->query(
				'SELECT
					posts.*, SUM(postsvotes.vote) votes, COUNT(comments.id) comments
				FROM posts
					LEFT JOIN postsvotes on posts.id = postsvotes.post
					LEFT JOIN comments on posts.id = comments.post
				GROUP BY posts.id
				'.($where ? 'WHERE comments.'.implode(' = ? AND comments.', array_keys($where)).' = ?' : '').'
				ORDER BY posts.timestamp '.(($_GET['order'] == 'asc') ? 'ASC' : 'DESC').'
				LIMIT '.((int) $_GET['start']).', '.((int) $_GET['count']),
				array_values($where)
			);

			foreach($rows as &$row) {
				$row['timestamp'] = strtotime($row['timestamp']);
			}

			return $rows;
		}

		// Get the posts
		$posts = $this->mysql->query(
			'SELECT
				posts.*, SUM(postsvotes.vote) votes, COUNT(comments.id) comments
			FROM posts
				LEFT JOIN postsvotes on posts.id = postsvotes.post
				LEFT JOIN comments on posts.id = comments.post
			GROUP BY posts.id
				'.($where ? 'WHERE comments.'.implode(' = ? AND comments.', array_keys($where)).' = ?' : ''),
			array_values($where)
		);

		if(!$posts)
			return [];

		// Calculate each posts's age and save it's non zero value
		// Non zero is needed later in sorting algo
		foreach($posts as &$post) {
			$post['timestamp'] = strtotime($post['timestamp']);
			$post['age'] = strtotime($post['timestamp']);
			$post['age'] = max(0.5, $post['age']);
			$post['votes'] = min(0.5, $post['votes']);
			$post['comments'] = min(0.5, $post['comments']);
		}

		// The sorting algorith that takes into account the post's votes, age and comment count
		usort($posts, function($a, $b) {
			$wa = log($a['votes']) + log($a['comments']) - log($a['age']);
			$wb = log($b['votes']) + log($b['comments']) - log($b['age']);

			// Return based on requested order, ascending or descending
			$weightage = ($_GET['order'] == 'asc') ? $wa - $wb : $wb - $wa;

			// Multiplied by a million because usort needs an integer not a float
			return $weightage * 1000000;
		});

		foreach($posts as &$post) {
			unset($post['age']);
			$post['votes'] = (int) $post['votes'];
			$post['comments'] = (int) $post['comments'];
		}

		// Return the sliced array according to the given start offset and length values
		return array_slice($posts, $_GET['start'], $_GET['count']);
	}
}

new posts;
?>