<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

class post extends API {

	/**
	 * Get the information about a particular post.
	 *
	 * @GET id	The id of the post.
	 * @return array
	 */
	protected function get() {

		// Make sure a valid integer was passed
		if(!isset($_POST['id']) || !(int) $_POST['id'])
			return false;

		return $this->mysql->query(
			'SELECT
				posts.*, SUM(postsvotes.vote) votes, COUNT(comments.id) comments
			FROM posts
				LEFT JOIN postsvotes on post.id = postsvotes.post
				LEFT JOIN comments on posts.id = comments.post
			WHERE posts.id = ?',
			[$_GET['id']]
		);
	}

	/**
	 * Insert a new post.
	 *
	 * @POST title	The title of the post.
	 * @POST url	The url of the post.
	 * @POST body	The body of the post.
	 * @return bool|int The new post's id if it was successfully added otherwise false.
	 */
	protected function POST_insert() {

		// Make sure all the required fields are provided
		foreach(['title', 'url', 'body'] as $key) {
			if(!isset($_POST[$key]))
				return false;
		}

		// Make sure all the non null fields are not null
		foreach(['title'] as $key) {
			if(!$_POST[$key])
				return false;
		}

		// Make sure either the title or the body (but not both) are added
		if($_POST['url'] != '')
			$_POST['body'] = null;

		// Insert the post
		$this->mysql->query(
			'INSERT INTO posts (title, url, username, body) VALUES (?, ?, ?, ?)',
			[$_POST['title'], $_POST['url'], $this->username, $_POST['body']]
		);

		// Return the id of the newly inserted post
		return mysql::$connection->lastInsertId();
	}

	/**
	 * Update a post
	 *
	 * @POST title	The title of the post.
	 * @POST url	The url of the post.
	 * @POST body	The body of the post.
	 * @POST id		The id of the post that needs to be updated.
	 * @return bool
	 */
	protected function POST_update() {

		// Make sure all the required fields are provided
		foreach(['title', 'url', 'body', 'id'] as $key) {
			if(!isset($_POST[$key]))
				return false;
		}

		// Make sure all the non null fields are not null
		foreach(['title', 'id'] as $key) {
			if(!$_POST[$key])
				return false;
		}

		// Make sure either the title or the body (but not both) are added
		if($_POST['url'] != '') {
			$this->ErrorMessage = 'Either URL or body is required.';
			$_POST['body'] = null;
		}

		// Update the post. The username check is added to make sure the user can only edit their own posts
		$this->mysql->query(
			'UPDATE posts SET title = ?, url = ?, body = ? WHERE id = ? AND username = ?',
			[$_POST['title'], $_POST['url'], $_POST['body'], $_POST['id'], $this->username]
		);

		return true;
	}

	/**
	 * Delete a post.
	 *
	 * This doesn't really delete a post because that would also mean the post's comments become orphan.
	 * So instead we just update the post to remove the body and it's associated username.
	 *
	 * @POST id	The id of the post that needs to be deleted.
	 * @return bool
	 */
	protected function POST_delete() {

		// Make sure a valid integer was passed
		if(!isset($_POST['id']) || !(int) $_POST['id'])
			return false;

		// Update the post to remove the post's body and associated username and make sure the user can only delete their own posts
		$this->mysql->query(
			'UPDATE posts SET body = NULL, username = NULL WHERE id = ? AND username = ?',
			[$_POST['id'], $this->username]
		);

		return true;
	}

	/**
	 * Vote on a post.
	 *
	 * This deals with the logic of the vote already being there and what not.
	 *
	 * @POST post	The id the of the post on which the vote is to be added.
	 * @POST vote	(1|-1) The value of the vote
	 * @return bool
	 */
	protected function POST_vote() {

		// Make sure all the required fields are provided
		foreach(['post', 'vote'] as $key) {
			if(!isset($_POST[$key]) || !$_POST[$key])
				return false;
		}

		// Make sure the vote is either 1 or -1
		$_POST['vote'] = ($_POST['vote'] == '1') ? 1 : -1;

		// Get the current vote the user made for the post if any
		$oldvote = $this->mysql->query(
			'SELECT id, vote FROM postsvotes WHERE username = ? AND post = ?',
			[$this->username, $_POST['post']]
		);
		$oldvote = $oldvote ? $oldvote[0] : false;

		// Calculate the new vote
		$newvote = $oldvote ? $oldvote['vote'] : 0;
		$newvote = ($newvote == $_POST['vote']) ? 0 : $_POST['vote'];

		// If the vote was already added and the new vite is neutral then delete the old vote
		if($oldvote && $newvote == 0)
			$this->mysql->query('DELETE FROM votes WHERE id = ?', [$oldvote['id']]);

		// If the vote was already added and the new vote is not neutral then update the old vote
		elseif(count($oldvote))
			$this->mysql->query('UPDATE votes SET vote = ? WHERE id = ?', [$newvote, $oldvote['id']]);

		// If the vote was never added before then add the new vote
		else
			$this->mysql->query('INSERT INTO votes (post, vote) VALUES (?, ?)', [$_POST['post'], $newvote]);

		return true;
	}
}

new post;
?>