<?php

/**
 * The class is used to make a quick query to the database.
 *
 * The class makes a query, shows any errors and logs the necessery queries.
 */
class mysql {

	/**
	 * @var PDOObject Holds the connection to the database.
	 */
	public static $connection = false;

 	/**
 	 * @var PDOStatement Holds the primary database call object.
 	 */
	public $call;

 	/**
 	 * @var array Holds the result of the query.
 	 */
 	public $rows;

 	/**
 	 * @var integer The number of database queries made on the current request.
 	 */
	public static $count = 0;

	/**
	 * Constructor to initiate the object and optionally execute a query.
	 *
	 * This can't return the rows because constructors return the object itself, so they can't return anything else.
	 *
	 * @uses mysql::query
	 *
	 * @param string|boolean	$query		The query that needs to be executed.
	 * @param array|boolean		$parameters	And array of parameters that will be bounded to the query.
	 */
	public function __construct($query = false, $parameters = false) {
		if($query)
			$this->query($query, $parameters);
	}

	/**
	 * Show an error when it occours.
	 *
	 * This takes care of the JSON API endpoints and shows JSON output when needed.
	 *
	 * @used-by mysql::query
	 *
	 * @param string $title			A quick summary of the error.
	 * @param string $description	JSON string describing the error in detail including optionally a call stack.
	 */
	function ShowError($title, $description = []) {

		// Pretty print the description and title
		$description = json_encode(array_merge(['title' => $title], $description), JSON_PRETTY_PRINT);

		// Needed to convert the literal newlines and tabs to corresponding ascii characters
		eval('$description = "'.addcslashes($description, '"').'";');

		exit($description);
	}

	/**
	 * This function loads the database connection info file and establishes a connection to the database.
	 *
	 * @used-by mysql::query
	 * @uses mysql::ShowError
	 */
	private function EstablishConnection() {

		global $PageConfig;

		if(!file_exists($PageConfig['ltr'].'includes/connection.php'))
			$this->ShowError('Database connection file doesn\'t exist.');

		// Include the file
		require($PageConfig['ltr'].'includes/connection.php');

		try {
			mysql::$connection = new PDO('mysql:host=127.0.0.1;dbname='.$database['name'], $database['user'], $database['password'], [PDO::ATTR_PERSISTENT => true]);
		} catch(Exception $e) {
			$this->ShowError('Error establishing database connection', $e);
		}
	}

	/**
	 * The function that actually makes the database call.
	 *
	 * The function returns an array of results or shows an error if necessery.
	 *
	 * @used-by mysql::__constructor
	 * @uses mysql::EstablishConnection
	 * @uses mysql::ShowError
	 *
	 * @param string		$query		The query string.
	 * @param array|boolean	$parameters	An optional array of parameters that will be bound to the query.
	 *
	 * @return array|boolean The result set if there was any.
	 */
	public function query($query, $parameters = false) {

		if(!$query)
			return false;

		if(!mysql::$connection)
			$this->EstablishConnection();

		// Prepare the query and set it to the call property
		$this->call = mysql::$connection->prepare($query);

		try {

			// Set a temporary error handler because try catch does not catch any warnings
			set_error_handler(function($number, $string) {throw new Exception($string);}, E_WARNING);

			// Execute the query
			$this->call->execute($parameters ? $parameters : null);

			// Remove the temporary error handler so that we don't mess with any other warnings that may be thrown
			restore_error_handler();

		} catch(Exception $e) {
			$title = substr($e->getMessage(), strpos($e->getMessage(), ']:') + 3);
			$description = ['Stack Trace' => $e->getTrace()];

			$this->ShowError($title, $description);
		}

		// The rows variable will hold an array of rows returned from the query
		$this->rows = ($this->call->columnCount() > 0) ? $this->call->fetchAll(PDO::FETCH_ASSOC) : [];

		$error = $this->call->errorInfo();

		// Show an error if the body is not null
		if($error[2] != '') {
			$description = [];

			// Remove the extra fluff in syntax errors
			$title = ($error[1] == 1064) ? substr($error[2], 128) : $error[2];

			$description['query'] = $query;

			// If any parameters were passed then prepare a list
			if($parameters)
				$description['parameters'] = $parameters;

			$this->ShowError($title, $description);
		}

		// Increment the query counter that is used to count the number of queries executed on the current page load
		mysql::$count++;

		return $this->rows;
	}
}