<?php

if(!isset($PageConfig))
	exit;

/**
 * The function takes n number of inputs and pretty prints them on the client.
 */
function debug() {
	foreach(func_get_args() as $argument) {
		echo '<pre>';

		if(is_array($argument) || is_object($argument))
			print_r($argument);
		else
			var_dump($argument);

		echo '</pre>';
	}
}

/**
 * Generates a random string of given length.
 * @param integer $size	The size of the string to be generated.
 * @return string		The random string.
 */
function RandomString($size) {
	for(
		$i = 0, $z = strlen($a = 'abcdefghijklmnopqrstuvwxyz1234567890')-1, $s = $a{rand(0,$z)}, $i = 1;
		$i != $size;
		$x = rand(0,$z), $s .= $a{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s)
	);
	return $s;
}

require_once($PageConfig['ltr'].'includes/api.class.php');
require_once($PageConfig['ltr'].'includes/mysql.class.php');
require_once($PageConfig['ltr'].'includes/password.include.php');