<?php

class API {

	/**
	 * @var PDOObject This is used to store a default mysql object for all api classes.
	 */
	protected $mysql;

	/**
	 * @var string Username of the currently logged in user.
	 */
	protected $username;

	/**
	 * @var string The user's authentication token.
	 */
	protected $token;

	/**
	 * @var string Used to store the error message of the api call.
	 */
	protected $ErrorMessage;

	/**
	 * This function sends the given array/object to the client and terminates.
	 *
	 * @param array|object|boolean $output The JSON that needs to be sent to the client.
	 */
	private function show($output, $ErrorMessage = false) {

		// Strict check to make sure empty arrays don't trigger the false status
		$status = $output !== false;

		if($ErrorMessage)
			$this->ErrorMessage = $ErrorMessage;

		$response = [
			'status' => $status,
			'response' => (!$status && $this->ErrorMessage) ? $this->ErrorMessage : $output
		];

		// Pretty print the JSON if requested
		if(isset($_GET['debug']))
			exit(json_encode($response, JSON_PRETTY_PRINT));
		else
			exit(json_encode($response));
	}

	/**
	 * The constructor takes the method name from the url and executes it.
	 */
	public function __construct() {

		// Send the json header
		header('Content-Type: application/json');

		// Make sure the action/method name was provided
		if(!isset($_GET['action']))
			$this->show(false, 'Action not provided!');

		if(!isset($_GET['token'])) {
			header("HTTP/1.1 401 Unauthorized");
			$this->show(false, 'Token Missing!');
		}

		// Set the authentication token
		$this->token = $_GET['token'];

		$this->mysql = new mysql;

		// Make sure the provided authentication token is valid
		if(!$this->ValidToken()) {
			header("HTTP/1.1 401 Unauthorized");
			$this->show(false, 'Invalid Token!');
		}

		// Append POST_ in front of the method if the request is POST
		$endpoint = ($_SERVER['REQUEST_METHOD'] == 'POST') ? 'POST_' : '';
		$endpoint .= $_GET['action'];

		// If the method name is valid then print it's return value
		if(method_exists($this, $endpoint))
			$this->show($this->{$endpoint}());
		else
			$this->show(false, 'Invalid Action!');
	}

	/**
	 * Checks wheather the provided token is valid or not.
	 *
	 * - Makes sure the token is valid.
	 * - Sets the class wide username variable of the validated user.
	 * - If the token was not used in past hour or the ip/ua changed then also updates the token.
	 *
	 * @param string $token The token that needs to be validated
	 * @return bool
	 */
	private function ValidToken() {

		$token = $this->mysql->query('SELECT * FROM tokens WHERE token = ?', [$this->token]);

		if(!$token)
			return false;

		$token = $token[0];

		// If the token is older than a week then it's invalid
		if(time() - strtotime($token['timestamp']) > 60 * 60 * 24 * 7) {

			// Delete the invalid token
			$this->mysql->query('DELETE FROM tokens WHERE token = ?', [$token]);

			return false;
		}

		$this->username = $token['username'];

		// If the token was not used in past hour or if the ip address or user-agent changed
		// TODO: Add the user agent check
		if(time() - strtotime($token['lastused']) > 60 * 60 || $token['ip'] != $_SERVER['REMOTE_ADDR'] || $token['useragent'] != $_SERVER['HTTP_USER_AGENT']) {

			// Update the ip address (and by implication last used time) of the token
			$this->mysql->query('UPDATE tokens SET ip = ?, useragent = ? WHERE id = ?', [$_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $token['id']]);
		}

		return true;
	}
}