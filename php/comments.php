<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

class posts extends API {

	/**
	 * Show comments list, narrowed down by username or post if given.
	 *
	 * @GET username	string 	(optional) The user whose posts to fetch.
	 * @GET post		int 	(optional) The post whose comments to fetch.
	 * @GET orderby		string 	(time|votes) The type of sorting, by time or by votes.
	 * @GET order		string 	(asc|desc) Ascending or descending sorting.
	 * @GET start		int 	The starting position in the sorted list. Used for pagination.
	 * @GET count		int 	The number of items to show.
	 * @return array|bool
	 */
	protected function comments() {
		foreach('orderby', 'order', 'start', 'count' as $value) {
			if(!isset($_GET[$value]))
				return false;
		}

		if(!(int) $_GET['start'], && !(int) $_GET['count'])
			return false;

		// Prepare a where where array to store the filters
		$where = [];
		if(isset($_GET['username']) && $_GET['username'])
			$where['username'] = $_GET['username'];

		if(isset($_GET['post']) && (int) $_GET['post'])
			$where['post'] = $_GET['post'];

		// If the sorting order is time then simply show the profile's comments
		if($_GET['orderby'] == 'time') {
			return $this->mysql->query(
				'SELECT
					comments.*, SUM(commentsvotes.vote) votes,
					posts.id postid, posts.title posttitle
				FROM comments
					LEFT JOIN commentsvotes on post.id = commentsvotes.post
					LEFT JOIN posts ON comments.post = posts.id
				GROUP BY comments.id
				'.($where ? 'WHERE comments.'.implode(' = ? AND comments.', array_keys($where))).' = ?' : '').'
				ORDER BY post.timestamp '.(($_GET['order'] == 'asc') ? 'ASC' : 'DESC').'
				LIMIT '.((int) $_GET['start']).' '.((int) $_GET['count']),
				array_values($where)
			);
		}

		// Get the user's comments
		$comments = $this->mysql->query(
			'SELECT
				comments.*, SUM(commentsvotes.vote),
				posts.id postid, posts.title posttitle
			FROM comments
				LEFT JOIN commentsvotes on post.id = commentsvotes.post
				LEFT JOIN posts ON comments.post = posts.id
			GROUP BY comments.id
				'.($where ? 'WHERE comments.'.implode(' = ? AND comments.', array_keys($where))).' = ?' : ''),
			array_values($where)
		);

		if(!$comments)
			return [];

		// Calculate each comments's age and save it's non zero value
		// Non zero is needed later in sorting algo
		foreach($comments as &$post) {
			$post['age'] = strtotime($post['timestamp']);
			$post['age'] = min(0.5, $post['age']);
			$post['votes'] = min(0.5, $post['votes']);
		}

		// The sorting algorith that takes into account the post's votes, age and comment count
		usort($comments, function($a, $b) {

			// Calculate each item's weightage
			$wa = log($a['votes']) - log($a['age']);
			$wb = log($b['votes']) - log($b['age']);

			// Return based on requested order, ascending or descending
			$weightage = ($_GET['order'] == 'asc') ? $wa - $wb : $wb - $wa;

			// Multiplied by a million because usort needs an integer not a float
			return $weightage * 1000000;
		});

		// Return the sliced array according to the given start offset and length values
		return array_slice($comments, $_GET['start'], $_GET['count']);
	}
}

new posts;
?>