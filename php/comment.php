<?php

$PageConfig['ltr'] = './';
require_once($PageConfig['ltr'].'includes/main.php');

class comment extends API {

	/**
	 * Insert a new comment.
	 *
	 * @POST body	The comment's body.
	 * @POST parent	The comment's parent.
	 * @POST post	The comment's post.
	 *
	 * @return bool
	 */
	protected function POST_insert() {

		// Make sure all the required fields are provided
		foreach(['body', 'parent', 'post'] as $key) {
			if(!isset($_POST[$key]))
				return false;
		}

		// Make sure all the non null fields are not null
		foreach(['post', 'body'] as $key) {
			if(!$_POST[$key])
				return false;
		}

		// Add the new comment
		$this->mysql->query(
			'INSERT INTO comments (username, body, parent, post) VALUES (?, ?, ?, ?)',
			[$this->username, htmlentities($_POST['body'], ENT_QUOTES), $_POST['parent'], $_POST['post']]
		);

		return true;
	}

	/**
	 * Update a comment.
	 *
	 * @POST id		The comment's id.
	 * @POST body	The comment's new body.
	 *
	 * @return bool
	 */
	protected function POST_update() {

		// Make sure all the required fields are provided
		foreach(['id', 'body'] as $key) {
			if(!isset($_POST[$key]) || !$_POST[$key])
				return false;
		}

		// Update the comment and make sure it belongs to the user
		$this->mysql->query(
			'UPDATE comments SET body = ? WHERE id = ? AND username = ?',
			[htmlentities($_POST['body'], ENT_QUOTES), $_POST['id'], $this->username]
		);

		return true;
	}

	/**
	 * Delete a comment.
	 *
	 * This doesn't really deletes a comment. It just empties the comment and removes the associated user.
	 * This is done to keep the comment heirarchey in place if a non leaf comment is deleted.
	 *
	 * @POST id	The comment's id.
	 *
	 * @return bool
	 */
	protected function POST_delete() {

		// Make sure all the required fields are provided
		foreach(['id'] as $key) {
			if(!isset($_POST[$key]) || !(int) $_POST[$key])
				return false;
		}

		// Update the comment and set it's body and username to null which signifies a deleted comment
		$this->mysql->query(
			'UPDATE comments SET body = NULL, username = NULL WHERE id = ? AND username = ?',
			[$_POST['id'], $this->username]
		);

		return true;
	}
}

new comment;
?>